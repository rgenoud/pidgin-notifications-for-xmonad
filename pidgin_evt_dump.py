#!/usr/bin/env python3
# Copyright (c) 2011 by Richard Genoud
#
# GNU General Public Licence (GPL)
# 
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
#

import sys
import time
import threading
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import GObject
from html.parser import HTMLParser
from pydbus import SessionBus

# this may be a command line parameter...
# it's use to clean xmobar afer x seconds
# 0 means no timeout
MESSAGE_TIMEOUT = 30


def show_message(account, sender, message, conversation, flags):
    periodic_timer.reset()
    print (sender, ":", strip_tags(message))
    sys.stdout.flush()

def clear_message():
    print ("")
    sys.stdout.flush()

# strip HTML tags from text
class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

class PerodicTimer:
    def __init__(self, timeout):
        self.value = timeout
        self.counter = self.value
        GLib.timeout_add_seconds(1, self.callback)

    def callback(self):
        # TODO: use a mutex to protect counter
	# ok, without one, it won't burn your house
        self.counter -= 1
        if (self.counter <= 0):
            clear_message()
            self.reset()
        return True

    def reset(self):
        self.counter = self.value


bus = SessionBus()
purple = bus.get("im.pidgin.purple.PurpleService", "/im/pidgin/purple/PurpleObject")

# some other events can be added here. (cf http://developer.pidgin.im/wiki/DbusHowto )
# a config file to select them would be nice
purple.ReceivedImMsg.connect(show_message)
purple.ReceivedChatMsg.connect(show_message)

if MESSAGE_TIMEOUT:
    periodic_timer = PerodicTimer(MESSAGE_TIMEOUT)

GLib.MainLoop().run()
